<?php
class Controller {
    public function __construct()
    {
    }
    public function isPost()
    {
       return $_SERVER['REQUEST_METHOD'] == 'POST';
    }
    public function isGet()
    {
        return $_SERVER['REQUEST_METHOD'] == 'GET';
    }
}
?>