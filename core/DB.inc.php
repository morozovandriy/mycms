<?php
class DB
{
    protected $pdo;
    public function __construct($user, $pass, $server, $database)
    {
        $dsn = "mysql:host={$server};database={$database}";
        $this->pdo = new PDO($dsn, $user, $pass);
    }
}