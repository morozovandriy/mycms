<?php
class ControllerResult {
    protected $baseTemplatePath;
    protected $params;
    public function __construct($path = 'templates/index.tpl')
    {
        $this->baseTemplatePath = $path;
        $this->params = array();
    }
    public function setTemplatePath($path)
    {
        $this->baseTemplatePath = $path;
    }
    public function getTemplatePath()
    {
        return $this->baseTemplatePath;
    }
    public function setParams($array)
    {
        $this->params = array_merge($this->params, $array);
    }
    public function getParams()
    {
        return $this->params;
    }
}
?>