<?php
require("config/database.inc.php");
class Core
{
    private static $instance;
    private $db;
    private $indexTPL;
    private function __construct()
    {
        session_start();
        $this->db = new DB(DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_SERVER, DATABASE_NAME);
    }
    public function __destruct()
    {

    }
    public function run($path)
    {
        $pathParts = explode('/', $path);
        $controllerName = ucfirst(array_shift($pathParts)).'_Controller';
        if (!empty($methodName = array_shift($pathParts)))
            $methodName = ucfirst($methodName).'Action';
        else
            $methodName = "IndexAction";
        if (class_exists($controllerName)) {
            $controllerObject = new $controllerName();
            if (method_exists($controllerObject, $methodName)) {
                $controllerRes = $controllerObject->$methodName($pathParts);
                $this->indexTPL = new Template($controllerRes->getTemplatePath());
                $this->indexTPL->setParams($controllerRes->getParams());
            }
            else
                echo "404 Not Found";
        } else
            echo "404 Not Found";
        $this->indexTPL->display();
    }
    public static function getInstance()
    {
        if (self::$instance == null)
            self::$instance = new Core();
        return self::$instance;
    }
}