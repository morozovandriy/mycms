<?php
class News_Model {
    public function getNewsById($id)
    {
        return $DB::getRowById('news', array('news_id' => $id));
    }
    public function updateNews($id, $rows)
    {
        $DB::update('news', array('news_id' => $id), $rows);
    }
}