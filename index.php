<?php
spl_autoload_extensions('.inc.php');
spl_autoload_register(function($className)
{
    $pathName = "./core/".$className.".inc.php";
    if (is_file($pathName))
        include($pathName);
});
spl_autoload_register(function($className)
{
    $arr = explode("_", $className);
    $folder = strtolower(array_shift($arr));
    $file = strtolower(array_shift($arr));
    $path = "modules/{$folder}/{$file}.inc.php";
    if (is_file($path)) {
        include($path);
        return;
    }
});
$core = Core::getInstance();
$core->run($_GET['path']);

